#include <iostream>
using namespace std;

struct Nod {
    int val;
    Nod *st, *dr;
};
class Tree {
    Nod * rad;

    void SRD(Nod *temp) {
        if(temp != NULL) {
            SRD(temp->st);
            cout<<temp->val<<" ";
            SRD(temp->dr);
        }
    }
public:
    Tree() {
        rad = NULL;
    }
    void add(int x) {
        Nod *toAdd = new Nod;   ///alocare memorie

        toAdd->val = x;
        toAdd->st = NULL;
        toAdd->dr = NULL;

        if(rad == NULL) {
            rad = toAdd;
        }
        else {
            Nod * temp = rad;
            while(1) {
                if(x < temp->val ) {         ///trebuie parcurs in st
                    if(temp->st == NULL) {  ///daca nu are nimic in st
                        temp->st = toAdd;
                        break;             ///conditie de iesire
                    }
                    else {                  ///daca are fiu stang
                        temp = temp->st;
                    }
                }
                if(x > temp->val) {         ///trebuie parcurs in dr
                    if(temp->dr == NULL) {  ///daca nu are nimic in dr
                        temp->dr = toAdd;
                        break;
                    }
                    else {                   ///daca are fiu drept
                        temp = temp->dr;
                    }
                }
            }
        }
    }

    void SRD() {
        SRD(rad);
    }

    void sterge(int x) {
        Nod *temp = rad;
        Nod *tata = rad;
        while(1){
            if(x < temp->val) {
                if(temp->st) {
                    tata = temp;
                    temp = temp->st;
                }
            }
            else if(x > temp->val) {
                if(temp->dr) {
                    tata = temp;
                    temp = temp->dr;
                }
            }
            else {
                break;
            }
        }
        ///temp este nodul de sters, tata este tatal


        ///daca nu are niciun fiu
        if(temp->st == NULL && temp->dr == NULL) {
            if( tata->val < temp->val) {    ///daca fiul era in dreapta , sterg legatura
                tata->dr = NULL;
            }
            else {
                tata->st = NULL;            ///daca fiul era in stanga, sterg legatura
            }
            delete temp;
        }
        ///daca are un fiu drept
        else if(temp->st == NULL && temp->dr ) {
            if( tata->val > temp->val) {    ///daca temp e fiu stang
                tata->st = temp->dr;
                delete temp;
            }
            else {                      ///daca temp e fiu drept
                tata->dr = temp->dr;
                delete temp;
            }
        }
        ///daca are un fiu stang
        else if(temp->st && temp->dr ==NULL ) {
            if(tata->val > temp->val) {  ///daca temp e fiu stang
                tata->st = temp->st;
                delete temp;
            }
            else {                      ///daca temp e fiu drept
                tata->dr = temp->st;
                delete temp;
            }
        }


        //if(temp->st && temp->dr ) {     ///daca are doi fii
        else {      ///daca are doi fii
            Nod * aux = temp->st;   ///cel mai mare fiu din stanga -> aux
            Nod *tataAux = temp->st;
            while( aux->dr ) {
                tataAux = aux;
                aux = aux->dr;
            }

            swap(temp->val, aux->val);  ///intersch valorile, in aux va fi ce trb sters
            ///aux poate avea un fiu stang sau niciunul ( in dr nu poate avea)

        }

    }

    bool cauta(int x) {
        while(rad) {
            if(rad->val == x){
                return 1;
            }
            else if(rad->val > x){
                rad = rad->st;
            }
            else if(rad->val < x){
                rad = rad->dr;
            }
        }
        return 0;
    }
    void inaltime(){
        cout<<findHeight(rad);
    }
    int findHeight(Nod* aNode) {
    if (aNode == NULL) {
        return -1;
    }

    int lefth = findHeight(aNode->st);
    int righth = findHeight(aNode->dr);

    if (lefth > righth) {
        return lefth + 1;
    } else {
        return righth + 1;
    }
}

};
int main()
{
    Tree t;
    t.add(20);
    t.add(10);
    t.add(5);
    t.add(2);
    t.add(7);
    t.add(6);
    t.add(12);
    t.add(30);
    t.add(25);
    t.add(35);
    t.add(45);
    t.SRD();
    cout<<endl;
    t.inaltime();
    return 0;
}
